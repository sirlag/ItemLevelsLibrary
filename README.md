ItemLevelsLibrary
----------
ItemLevelsLibrary is an open source library used to connect to the Final Fantasy XIV Lodestone, and gather item
information about the target player that is otherwise hidden (Primarily Item Level). Eventually, this library will be
used in a public facing web app.