package moe.dissipate;

public class Player {
    private String URL, Name, image, server;

    public String getURL() {
        return URL;
    }

    public String getName() {
        return Name;
    }

    public String getImage() {
        return image;
    }

    public String getServer() {
        return server;
    }

    public Player(String URL, String name, String image, String server) {
        this.URL = URL;
        Name = name;
        this.image = image;
        this.server = server;
    }

    @Override
    public String toString() {
        return "Player{" +
                "URL='" + URL + '\'' +
                ", Name='" + Name + '\'' +
                ", image='" + image + '\'' +
                ", server='" + server + '\'' +
                '}';
    }
}
