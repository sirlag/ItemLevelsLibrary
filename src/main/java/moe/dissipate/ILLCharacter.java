package moe.dissipate;

import java.util.HashSet;

public class ILLCharacter {
    private String Name;
    private HashSet<ILLItem> items;

    public int totalItemLevel(){
        int ilvl = 0;
        if (items.size() == 12)
            if (items.stream().findFirst().isPresent())
                ilvl += items.stream().findFirst().get().getItemLevel();
        for(ILLItem item : items){
            ilvl +=  item.getName().matches("Soul of the") ? 0: item.getItemLevel();
        }
        return ilvl;
    }

    public int averageItemLevel(){
        return totalItemLevel()/13;
    }

    public ILLCharacter(String name, HashSet<ILLItem> items) {
        Name = name;
        this.items = items;
    }

    public String getName() {
        return Name;
    }

    public HashSet<ILLItem> getItems() {
        return items;
    }
}
