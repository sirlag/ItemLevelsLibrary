package moe.dissipate.examples;

import moe.dissipate.ILLCharacter;
import moe.dissipate.ILLItem;
import moe.dissipate.Player;
import moe.dissipate.PlayerSearch;

/*
    This program is a sample of how to chain this library together in order to get decent results.
 */
public class ExampleProgram {
    public static void main(String[] args){
        Player ourPlayer = PlayerSearch.Search("Li Austrene", "Phoenix").iterator().next();
        System.out.println(new ILLCharacter(ourPlayer.getName(), ILLItem.getItems(ourPlayer.getURL())).averageItemLevel());
    }
}
